import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: [],
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ locations: data.locations });

        }
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        delete data.locations
        console.log(data)

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            this.setState({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            });
        }
    }



    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({ starts: value })
    }
    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ ends: value })
    }
    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({ description: value })
    }
    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({ max_presentations: value })
    }
    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({ max_attendees: value })
    }
    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }



    render() {

        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new conference</h1>
                            <form onSubmit={this.handleSubmit} id="create-conference-form">
                                <div className="form-floating mb-3">
                                    <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                                        className="form-control" />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.starts} onChange={this.handleStartsChange} placeholder="start_date" required type="date" name="starts" id="starts"
                                        className="form-control" />
                                    <label htmlFor="start_date">Starts</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.ends} onChange={this.handleEndsChange} placeholder="end_date" required type="date" name="ends" id="ends"
                                        className="form-control" />
                                    <label htmlFor="end_date">Ends</label>
                                </div>
                                <div className="mb-3">
                                    <label className="form-label">Description</label>
                                    <textarea value={this.state.description} onChange={this.handleDescriptionChange} className="form-control" id="description" rows="3"></textarea>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.max_presentations} onChange={this.handleMaxPresentationsChange} placeholder="max_presentations" required type="max_presentations"
                                        name="max_presentations" id="max_presentations" className="form-control" />
                                    <label htmlFor="max_presentations">Maximum presentations</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input value={this.state.max_attendees} onChange={this.handleMaxAttendeesChange} placeholder="max_attendees" required type="max_attendees" name="max_attendees"
                                        id="max_attendees" className="form-control" />
                                    <label htmlFor="max_attendees">Maximum attendees</label>
                                </div>
                                <div className="mb-3">

                                    <select value={this.state.location} onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                                        <option value="">Choose a location</option>
                                        {this.state.locations.map(location => {
                                            return (
                                                <option key={location.id}
                                                    value={location.id}>
                                                    {location.name}
                                                </option>
                                            )
                                        })}
                                    </select>

                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ConferenceForm;


